from django.forms import model_to_dict
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from demoapp.models import Widget
from demoapp.tasks import create_widgets


class WidgetListView(APIView):

    def get(self, request, **kwargs):
        widget_list = Widget.objects.get_widgets()
        create_widgets.delay()
        return Response({
            'data': widget_list,
            'code': "WL200",
            'message': "Widget List Found"
        }, status=status.HTTP_200_OK)
