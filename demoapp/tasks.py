from celery import shared_task
from celery.utils.log import get_task_logger

from demoapp.models import Widget


logger = get_task_logger(__name__)


@shared_task
def add(x, y):
    return x + y


@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)


@shared_task
def create_widgets():
    logger.info("Widget added")
    return Widget.objects.create_widgets()
