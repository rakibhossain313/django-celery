# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Widget


@admin.register(Widget)
class WidgetAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)
