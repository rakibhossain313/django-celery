import random

from django.db import models


class WidgetManager(models.Manager):

    def get_widgets(self):
        return self.filter().values('id', 'name')

    def count_widgets(self):
        return self.count()

    def create_widgets(self):
        self.create(name=random.randint(2345678909800, 9923456789000))
        # return self.get_widgets()

    def rename_widget(self, widget_id, name):
        w = self.get(id=widget_id)
        w.name = name
        w.save()


class Widget(models.Model):
    name = models.CharField(max_length=140)
    objects = WidgetManager()
