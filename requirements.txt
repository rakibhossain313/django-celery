amqp==5.0.6
anyjson==0.3.3
asgiref==3.2.10
backcall==0.2.0
billiard==3.6.4.0
celery==5.1.2
certifi==2020.6.20
chardet==3.0.4
click==7.1.2
click-didyoumean==0.0.3
click-plugins==1.1.1
click-repl==0.2.0
configparser==5.0.0
coreapi==2.3.3
coreschema==0.0.4
decorator==4.4.2
Django==3.1.2
django-celery==3.3.1
django-celery-results==2.2.0
django-cors-headers==3.5.0
django-environ==0.4.5
django-extensions==3.0.9
django-redis==5.0.0
djangorestframework==3.12.1
djangorestframework-jwt==1.11.0
idna==2.10
inflection==0.5.1
ipython==7.18.1
ipython-genutils==0.2.0
itypes==1.2.0
jedi==0.17.2
Jinja2==2.11.2
kombu==5.1.0
libscrc==1.5
MarkupSafe==1.1.1
minio==6.0.0
numpy==1.21.2
openapi-codec==1.3.2
packaging==20.4
pandas==1.2.3
parso==0.7.0
pexpect==4.8.0
pickleshare==0.7.5
Pillow==7.2.0
prompt-toolkit==3.0.7
psycopg2-binary==2.8.6
ptyprocess==0.6.0
Pygments==2.7.1
PyJWT==1.7.1
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2020.1
qrcode==6.1
redis==3.5.3
requests==2.24.0
ruamel.yaml==0.16.12
ruamel.yaml.clib==0.2.2
simplejson==3.17.2
six==1.15.0
sqlparse==0.3.1
tqdm==4.50.0
traitlets==5.0.4
ulid-py==1.1.0
uritemplate==3.0.1
urllib3==1.25.10
vine==5.0.0
wcwidth==0.2.5
